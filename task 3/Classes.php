<?php
/*
Реализовать на PHP структуру классов, описывающих следующие фигуры: прямоугольник, круг, треугольник.
Описать функцию для нахождения площади фигуры.
Реализовать:

1) генерацию случайных объектов классов с заполнением полей случайными значениями
2) сохранение объектов в файл в любом удобном представлении
3) получение объектов из файла

Отсортировать полученную коллекцию объектов по убыванию площади фигуры и вывести результат на экран.
*/
    class Figure
    {
        protected $figure = ""; // тип фигуры
        public function getArea() {} // функция вычисления площади
        public function typeOfFigure() // функция получения типа фигуры
        {
            if ($this->figure == "") {
                return "Undefined";
            }
            else {
                return $this->figure;
            } 
        }
    }

    class Rectangle extends Figure 
    {
        private $width;
        private $height;
        public function getRectSides() // функция получения сторон прямоугольника
        {
            return array($this->width, $this->height);
        }
        function __construct($_width = 0, $_height = 0)
        {
            $this->figure = "";            
            if (($_width > 0) && ($_height > 0)) {
                $this->figure = "R";
                $this->width = $_width;
                $this->height = $_height;
            }
        }
        public function getArea()
        {
            if (!$this->figure) {
                return "";
            }
            return $this->width * $this->height;
        }
    }

    class Circle extends Figure 
    {
        private $radius; 
        public function getRadius() // функция получения радиуса окружности
        {
            return  $this->radius;
        }
        function __construct($_radius = 0)
        {
            $this->figure = ""; 
            $_radius = floatval($_radius);           
            if ($_radius > 0) {
                $this->figure = "C";
                $this->radius = $_radius;
            }
        }
        public function getArea()
        {
            if (!$this->figure) {
                return "";
            }
            return (pow($this->radius, 2) * M_PI);
        }
    }

    class Triangle extends Figure 
    {
        private $first_side;
        private $second_side;
        private $third_side; 
        public function getSides() // функция получения 3х сторон треугольника
        {
            return array($this->first_side, $this->second_side, $this->third_side);
        }
        function __construct($_first_side = 0, $_second_side = 0, $_third_side = 0)
        {
            $this->figure = "";   
            $_first_side = floatval($_first_side);
            $_second_side = floatval($_second_side);
            $_third_side = floatval($_third_side);         
            if (($_first_side > 0) && ($_second_side > 0) && ($_third_side > 0)) {
                $half_p = (($_first_side + $_second_side + $_third_side) / 2); // полупериметр
                if (($half_p > $_first_side ) && ($half_p > $_second_side) && ($half_p > $_third_side)) {
                    $this->figure = "T";
                    $this->first_side = $_first_side;                    
                    $this->second_side = $_second_side;  
                    $this->third_side = $_third_side;  
                }
            }
        }
        public function getArea()
        {
            if (!$this->figure) {
                return "";
            }
            $half_p = (($this->first_side + $this->second_side + $this->third_side) / 2); // полупериметр
            $tmp1 = $half_p - $this->first_side;
            $tmp2 = $half_p - $this->second_side;
            $tmp3 = $half_p - $this->third_side;
            return (sqrt($half_p  * $tmp1 * $tmp2 * $tmp3));
        }
    }

    function createNewFigures(int $n = 0) {
        if ($n > 0) {  
            $new_figures = array();           
            for ($i = 0; $i < $n; $i++) {
                $type_num = rand(1,3);
                switch($type_num) {
                    case 1: {
                        $tmp1 = rand(1,10);
                        $new_figure = new Circle($tmp1);
                        array_push($new_figures, $new_figure); 
                        break;
                    }
                    case 2: {
                        $tmp1 = rand(1,10);
                        $tmp2 = rand(1,10); 
                        $new_figure = new Rectangle($tmp1, $tmp2);
                        array_push($new_figures, $new_figure);
                        break;
                    }
                    case 3: {
                        $tmp1 = rand(1,10);
                        $tmp2 = rand(1,10);
                        $tmp3 = rand(1,10);
                        $new_figure = new Triangle($tmp1, $tmp2, $tmp3);
                        array_push($new_figures, $new_figure); 
                        break;
                    }
                }  
            }
            return $new_figures;
        }
        else {
            echo "Wrong input!!!";
        }
    }

    function saveFiguresToFile($arr) {
        $f = fopen("figures.txt","a+t");
        foreach ($arr as &$value) {
            switch($value->typeOfFigure()) {
                case 'C': { 
                    
                    fwrite($f, "C {$value->getRadius()}\n"); 
                    break;
                }
                case 'R': {
                    $tmp_arr = $value->getRectSides();
                    fwrite($f, "R {$tmp_arr[0]} {$tmp_arr[1]}\n"); 
                    break;
                }
                case 'T': { 
                    $tmp_arr = $value->getSides();
                    fwrite($f, "T {$tmp_arr[0]} {$tmp_arr[1]} {$tmp_arr[2]}\n"); 
                    break;
                }
            }
        }
        fclose($f);
    }

    function readFiguresFromFile() {
        $f = fopen("figures.txt","r");
        $fig_arr = array();
        while(!feof($f)) {
            $arr = explode(' ',trim(fgets($f)));
            switch($arr[0]) {
                case 'C': { 
                    $new_figure = new Circle($arr[1]);
                    array_push($fig_arr, $new_figure); 
                    break;
                }
                case 'R': {  
                    $new_figure = new Rectangle($arr[1], $arr[2]);
                    array_push($fig_arr, $new_figure);
                    break;
                }
                case 'T': { 
                    $new_figure = new Triangle($arr[1], $arr[2], $arr[3]);
                    array_push($fig_arr, $new_figure); 
                    break;
                }
            }                       
        }       
        return $fig_arr;
    }

    function sortFiguresByArea($arr) {
        $sort_arr = array();
        foreach ($arr as &$value) {
            $sort_arr[floatval($value->getArea())] = $value;
        }
        ksort($sort_arr);
        echo '<pre>';
        print_r($sort_arr);
        echo '<pre>';        
    }    
