<?php
    function printFirst64Fibonacci() {
        echo "This program will print first 64 Fibonacci numbers: <br>";
        $first_num = 0;
        $next_num = 1;
        $current_num = 0;
        for ($i = 0; $i < 64; $i++) {
            if ($i == 0) {
                echo $first_num . '<br>';
            }
            elseif ($i == 1) {
                echo $next_num . '<br>';
            }
            else {
                $current_num = $first_num + $next_num;
                $first_num = $next_num;
                $next_num = $current_num;
                echo $current_num . '<br>';
            }
        }
    }
